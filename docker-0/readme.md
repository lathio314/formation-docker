# Docker

- Permet de créer des environnements (appelés conteneurs) de manière à isoler des applications
- Il permet d'embarquer une application ainsi que les dépendances nécessaires dans un conteneur virtuel isolé qui pourra être exécuté sur n'importe quelle machine supportant docker

## Image

- est un fichier package représentant la template de conteneurs
- Elle définit la struture du conteneur en englobe l'application containérisée et l'ensemble de ses dépendances

## Conteneur

- Représente une instance d'une image
- Enveloppe permettant de packager une application avec juste ce dont elle a besoin pour fonctionner
- Peut être déployé tel quel dans n'importe quelle machine disposant d'un container engine avec différents environnements (Dev, Test, Prod)
- Utilise le Kernel de l'OS Hôte
- A son propre espace de processus et sa propre interface réseau
- Isolé de l'Hôte, mais exécutée directement dessus
- Permet de décomposer l'insfrastructure applicative en petits éléments légers facile à déployer et à réutiliser