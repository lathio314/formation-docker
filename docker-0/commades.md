# Fromations docker

## Commandes utiles

### docker --version

### docker info

## docker logs -f xxx

### docker system prune

### docker ps

- Lister les conteneurs actifs, les conteneurs en cours d'exécution
- l'options -a ou --all permet de lister tous les conteneurs actifs et non
- on peut utlise à la place
  - docker ps -q ou docker container ls
    - pour lister les conteneurs en cours d'exécution
  - docker ps -qa ou docker container ls -a
    - pour lister tous les conteneurs actifs et non

### docker run [options] image:tag

- Pour lancer(activer) un conteneur
- [options]
  - -d
    - mode détaché
  - -p hostPort:containerPort
    - Pour mapper le port du conteneur au port de la machine hôte
    - hostPort représente le port de la machine hôte
    - containerPort représente le port du conteneur
  - -v hostVolume:containerVolume
  - -e var=value
  - --name containername
  - --network networkName
  - -ti
  - --rm

## docker exec -it #containerId bash|sh

### docker stop #containerId

### docker start #containerId

### docker restart #containerId

### docker kill #containerId

### docker rm #containerId

### docker inspect #containerId

### docker images

### docker pull imageName:tag

### docker rmi #imageId

## docker login -u username -p password
