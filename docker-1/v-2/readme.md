#

## Lancer d'abord la commande ci-dessous pour créer un nouveau projet angular

    ng new ang-v-2 --ssr false --routing true --style scss

## Déplacez vous dans le dossier ang-v-2

    cd ang-v-2

## Copier les deux fichiers Dockerfile et .dockerignore dans le dossier ang-v-2

## Lancer la commande

    docker build -t ang-v-2 .

    docker run -d --name ang2 -p 8080:80 --rm ang-v-2
