#

## Lancer d'abord la commande ci-dessous pour créer un nouveau projet angular

    ng new ang-v-1 --ssr false --routing true --style scss

## Déplacez vous dans le dossier ang-v-1

    cd ang-v-1

## Copier les deux fichiers Dockerfile et .dockerignore dans le dossier ang-v-1

## Lancer la commande

    docker build -t ang-v-1 .

    docker run -d --name ang1 -p 4202:4200 --rm ang-v-1
