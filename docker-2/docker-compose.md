# Format d'un docker-compose.yml ou docker-compose.yaml

## version: "1-3.8 | name: nomFile"

## services:

        service1:
           image: imagedebase
           container_name: nomduconteneur
           build:
             context: 
             dockerfile: Dockerfile
           ports:
             - "8080:80"
             - 7000:5000
           restart: always
           environment:
             MYSQL_ROOT_PASS: root
             MYSQL_DATABASE: db_mysql
           volumes:
             - ./data:/containerPath
             - ./data2:/containerPath2
           networks:
             - net1
             - net2
           depends_on: 
        
        service2:

        serviceN:

## volumes:

## networks: