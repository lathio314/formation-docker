const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());


app.get('/', (req, res) => {
    res.send('Bienvenue sur mon API REST modifié ppp hhh');
});

app.get('/api/ressource', (req, res) => {
    res.json({ message: 'Cette route renvoie des données.' });
});

app.post('/api/ressource', (req, res) => {
    const data = req.body;
    res.json({ message: 'Données reçues avec succès.', data });
});

app.listen(port, () => {
    console.log(`Le serveur écoute sur le port ${port}`);
});
